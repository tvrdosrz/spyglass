# spyglass

1. create a venv
    ```
    python -m venv .venv
    ```
2. install requirements
    ```
    pip install -r requirements.txt
    ```
3. run with 
    ```
    python spyglass.py
    ```
