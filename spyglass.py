#!/usr/bin/python

import os
import yaml
import platform
from pathlib import Path
# from rich import print

import typer


app = typer.Typer()
project_subcommand = typer.Typer()
app.add_typer(project_subcommand, name='project')

entries: list = []


@project_subcommand.command('add')
def project_add(path: Path, name: str = None, runner: str = None, editor: str = 'code', multi: bool = False):

    global entries

    if multi:
        if name:
            print('\n[bold red]Cannot name multiple projects the same.\n')
            exit(69)

        for dir in os.listdir(path):
            print(dir)
            entries.append(assamble_entry(path/dir, name, runner, editor))
    else:
        entries.append(assamble_entry(path, name, runner, editor))

    for project in entries:
        store_project(project)





def get_database_path():
    database: str = None
    os_type: str = platform.system()

    if os_type == 'Linux':
        database = os.environ.get('HOME') + '/.local/share/spyglass-db.yaml'
    elif os_type == 'Windows':
        database = os.environ.get('APPDATA') + 'spyglass-db.yaml'
    else:
        print('OS not supported')
        exit(69)

    return database



def store_project(project: dict):
    database: str = get_database_path()

    if not os.path.exists(database):
        with open(database, 'w') as f:
            f.write('projects: []')
    data = None
    with open(database, 'r') as f:
        data = yaml.safe_load(f)

    project_list: list = [proj for proj in data['projects'] if os.path.isdir(proj)]

    print(project_list)

    if project not in project_list:
        project_list.append(project)
    else:
        print('Project already exists')

    data = { 'projects' : project_list }

    with open(database, 'w') as f:
        yaml.safe_dump(data, f)


def assamble_entry(path: Path, name: str, runner: str, editor: str):
    if not name:
        name = os.path.dirname(path)

    if not runner:
        runner = 'None'

    if not editor:
        editor = 'code'

    return {name: {'path': str(path), 'runner': str(path/runner), 'editor': editor}}



def main():
    app()


if __name__ == '__main__':
    main()
